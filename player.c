#include "bot.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>
//#include <time.h>

double myx;
double myy;



int food(const void *a, const void *b)
{
    double ax = (*(struct food *)a).x;
    double ay = (*(struct food *)a).y;
    double bx = (*(struct food *)b).x;
    double by = (*(struct food *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

int cellsort(const void *a, const void *b)
{
    double ax = (*(struct cell *)a).x;
    double ay = (*(struct cell *)a).y;
    double bx = (*(struct cell *)b).x;
    double by = (*(struct cell *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
}

int playersort(const void *a, const void *b)
{
    double ax = (*(struct player *)a).x;
    double ay = (*(struct player *)a).y;
    double bx = (*(struct player *)b).x;
    double by = (*(struct player *)b).y;
    
    double dist_to_a = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double dist_to_b = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dist_to_a - dist_to_b;
    
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
     // Copy player's x-y to globals
    myx = me.x;
    myy = me.y;
    
    // Sort food by distance
    qsort(foods, nfood, sizeof(struct food), food);
    // sort virus and mass by distance
    qsort(virii, nvirii, sizeof(struct cell), cellsort);
    qsort(mass, nmass, sizeof(struct cell), cellsort);
    qsort(players, nplayers, sizeof(struct player), playersort);
    
    
    double dist_to_virus = sqrt((me.x-virii[0].x)*(me.x-virii[0].x)+(me.y-virii[0].y)*(me.y-virii[0].y));
    double dist_to_mass = sqrt((me.x-mass[0].x)*(me.x-mass[0].x)+(me.y-mass[0].y)*(me.y-mass[0].y));
    double dist_to_food = sqrt((me.x-foods[0].x)*(me.x-foods[0].x)+(me.y-foods[0].y)*(me.y-foods[0].y));
    double dist_to_player = sqrt((me.x-players[0].x)*(me.x-players[0].x)+(me.y-players[0].y)*(me.y-players[0].y));
    
    
    if((players[0].totalMass<(5/6)*me.totalMass) && dist_to_player<=1000){                  //goes towards nearest player if size less then bot
        act.dx = players[0].x - me.x;
        act.dy = players[0].y - me.y;
        act.fire = 0;
        act.split = 0;
        if((players[0].totalMass<(1/3)*me.totalMass) && act.split == 0){         //if player is less then a thir of size of bot split
            act.dx = players[0].x - me.x;
            act.dy = players[0].y - me.y;
            act.fire = 0;
            act.split = 1;
        }
    }
    else{
        if(players[0].totalMass>me.totalMass){                          //run away from player if bigger
            act.dx = me.x - players[0].x;                                     
            act.dy = me.y - players[0].y;
            act.fire = 0;
            act.split = 0;
        }
        else{
            if(dist_to_virus<100){                              //Avoids virus *doesn't really work once the bot gets big
                    act.dx = (me.x - virii[0].x)*100;           //bot confusion increases with distance so 100 seems like a good medium
                    act.dy = (me.y - virii[0].y)*100;
                    act.fire = 0;
                    act.split = 0;
            }
            else {
                if(dist_to_mass>=1000 && dist_to_food < dist_to_virus){          //gets food
                    act.dx = foods[0].x - me.x;
                    act.dy = foods[0].y - me.y;
                    act.fire = 0;
                    act.split = 0;
                }
                else{ 
                    if(dist_to_mass<1000 && dist_to_mass < dist_to_virus){
                        act.dx = (mass[0].x - me.x);                  //get mass if its within 1000
                        act.dy = (mass[0].y - me.y);
                        act.fire = 0;
                        act.split = 0;
                    }
                    else{
                        int randx = rand() % 201 + (-100);
                        int randy = rand() % 201 + (-100);
                        act.dx = randx;
                        act.dy = randy;                             //bot randomly moves around if nothing to do
                        act.fire = 0;
                        act.split = 0;
                    }
                }
            }
        }
    }
    
    
    

    

    
    
    
    // Move toward closest food
    
    return act;
}
